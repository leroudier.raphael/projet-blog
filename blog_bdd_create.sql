-- CREATION DE LA BASE DE DONNÉES
-- CREATE DATABASE blog_bdd;

-- --CREATION DE MES 3 TABLES
-- USE blog_bdd;
-- 
CREATE TABLE Post(
	id INT PRIMARY KEY NOT NULL auto_increment,
    title VARCHAR(65),
    author VARCHAR(65),
    postDate DATETIME DEFAULT current_timestamp, 
    content varchar (250),
    imgPath varchar (250)
);
CREATE TABLE Comment(id INT PRIMARY KEY NOT NULL auto_increment,contentPost VARCHAR(65),author VARCHAR(65), post_id INT);
ALTER TABLE Comment ADD FOREIGN KEY (post_id) REFERENCES Post(id);
CREATE TABLE Note(id int primary key not null auto_increment,note int, post_id int);
ALTER TABLE Note ADD FOREIGN KEY (post_id) REFERENCES Post(id);
