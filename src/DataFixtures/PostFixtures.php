<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class PostFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        for ($i=0; $i < 10; $i++) { 
            $title ='Title '.$i;
           $post = new Post($faker->userName, $title, $faker->dateTime, $faker->text(200));
           $post->setImgPath('https://image.flaticon.com/icons/svg/17/17004.svg');
                $manager->persist($post);
            }

        $manager->flush();
    }
}
