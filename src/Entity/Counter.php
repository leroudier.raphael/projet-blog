<?php

namespace App\Entity;

class Counter
{
    private $count_id;
    private $id;
    public function __construct(int $count_id, int $id = null)
    {
        $this->$count_id = $count_id;
        $this->id = $id;
    }
    public function getPost_id(): int
    {
        return $this->count_id;
    }
    public function getId(): int
    {
        return $this->id;
    }
    public function setPost_id($count_id): void
    {
        $this->count_id = $count_id;
    }
    public function setId($id): void
    {
        $this->id = $id;
    }
}
