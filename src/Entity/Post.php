<?php
namespace App\Entity;


class Post {
    private $id;
    private $title;
    private $author;
    private $postDate;
    private $content;
    private $imgPath;

    public function __construct(string $title,string $author, \DateTime $postDate, string $content, string $imgPath = null, int $id = null) {
        $this->id = $id;
        $this->title = $title;
        $this->author = $author;
        $this->postDate= $postDate;
        $this->content = $content;
        $this->imgPath = $imgPath;
    }

    public function getTitle():string {
        return $this->title;
    }
    public function getAuthor():string {
        return $this->author;
    }
    public function getPostDate():\DateTime {
        return $this->postDate;
    }
    public function getContent():string {
        return $this->content;
    }
    public function getImgPath():string {
        return $this->imgPath;
    }
    public function getId():int {
        return $this->id;
    }
    public function setAuthor(string $author): void {
        $this->author = $author;
    }
    public function setTitle(string $title): void {
        $this->title = $title;
    }
    public function setPostDate(\dateTime $postDate): void {
        $this->postDate = $postDate;
    }
    public function setContent(string $content): void {
        $this->content = $content;
    }
    public function setImgPath(string $imgPath): void {
        $this->imgPath = $imgPath;
    }
    public function setId(int $id): void {
        $this->id = $id;
    }
}