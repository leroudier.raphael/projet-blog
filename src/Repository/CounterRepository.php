<?php

namespace App\Repository;

use App\Entity\Counter;

class CounterRepository {
    private $pdo;

    public function __construct() {
    
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].
            ';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }

    /**
     * 
     * @return Counter[] 
     */
    public function findAll(): array
    {
        $query = $this->pdo->prepare('SELECT * FROM Counter');
        $query->execute();

        $results = $query->fetchAll();
        $list = [];
        foreach ($results as $line) {
 
            $post = $this->sqlToPost($line);
            $list[] = $post;
        }

        return $list;
    }
   


    public function updateCounter($count, $id): void {
       
        $query = $this->pdo->prepare ('UPDATE Counter
        SET count_id = :countId WHERE id = :id ');
     
        $query->bindValue(':countId', $count, \PDO::PARAM_STR);

        $query->bindValue(':id', $id, \PDO::PARAM_INT);

        $query->execute();

   }

    public function findById(int $id): ?Counter {
        $query = $this->pdo->prepare('SELECT * FROM Counter WHERE id=:idPlaceholder');
   
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        $query->execute();
        $line = $query->fetch();
        if($line) {
            return $this->sqlToPost($line);
        }
       
        return null;

    }

    private function sqlToPost(array $line):Counter {
        return new Counter($line['post_id'], $line['id']);
    }

}
