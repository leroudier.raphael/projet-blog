<?php

namespace App\Repository;

use App\Entity\Post;

class PostRepository {
    private $pdo;

    public function __construct() {
        /**
         * On fait une instance de PDO dans le constructeur
         * car on utilisera cette connexion dans toutes les
         * méthodes de cette classe (techniquement, il faudrait
         * même la mettre dans une classe à part dédiée à la
         * création de la connexion)
         * 
         * L'instance de PDO représente une connexion à la base de
         * données. Elle attend en argument le serveur sur laquelle
         * se trouve notre bdd (localhost), le nom de la bdd à laquelle
         * on veut se connecter (introduction), puis le username et le
         * password pour se connecter à cette base de données. (il 
         * serait préférable de mettre ces informations dans un fichier
         * de configuration hors du php)
         */
        $this->pdo = new \PDO(
            'mysql:host='.$_ENV['DATABASE_HOST'].
            ';dbname=' . $_ENV['DATABASE_NAME'],
            $_ENV['DATABASE_USERNAME'],
            $_ENV['DATABASE_PASSWORD']
        );
    }

    /**
     * Méthode qui va aller chercher toutes les post
     * présentent dans la base de données et les convertir
     * en instance de la classe Post
     * @return Post[] les post contenus dans la bdd
     */
    public function findAll(): array
    {
        //On crée la requête SQL en utilisant la méthode prepare de pdo
        $query = $this->pdo->prepare('SELECT * FROM Post');
        //on exécute la requête
        $query->execute();
        /**
         * On récupère tous les résultats sous forme
         * tableau associatifs
         */
        $results = $query->fetchAll();
        $list = [];
        //On fait une boucle sur les résultats
        foreach ($results as $line) {
            /**
             * A chaque ligne de résultat, on crée une instance de la 
             * classe Post en lui donnant comme arguments les données
             * contenues dans chaque ligne
             */
            $post = $this->sqlToPost($line);
            //On met le post créé dans un tableau
            $list[] = $post;
        }
        //On renvoie le tableau de Person
        return $list;
    }
    /**
     * Méthode permettant de faire persister une instance de Post
     * en base de données
     */
    public function add(Post $post): void {
        /**
         * On crée la requête pour faire une insertion, et dans cette
         * requête pour les valeurs à insérer dans la bdd, on va plutôt
         * mettre des placeholders (:title, :author ...) qu'on
         * pourra assigner par la suite
         */
        $query = $this->pdo->prepare('INSERT INTO Post (title,author,content,imgPath) VALUES (:title,:author,:content,:imgPath)');
        //On assigne chaque valeur à faire persister venant de l'instance
        //de la classe Post aux placeholders définis dans la requête.
        //On peut également précisé en troisième argument le type SQL attendu
        $query->bindValue(':title', $post->getTitle(), \PDO::PARAM_STR);
        $query->bindValue(':author', $post->getAuthor(), \PDO::PARAM_STR);
        $query->bindValue(':content', $post->getContent(), \PDO::PARAM_STR);
        $query->bindValue(':imgPath', $post->getImgPath(), \PDO::PARAM_STR);
        //On exécute la requête
        $query->execute();
        //On assigne l'id généré par SQL à l'instance de Post
        $post->setId(intval($this->pdo->lastInsertId()));
        

    }
    /**
     * Méthode permettant de supprimer un Post
     * en base de données e nrecupérant son id
     */
    public function delete(int $id) :void{
        $query = $this->pdo->prepare('DELETE FROM `Post` WHERE `id` = :id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
    }

        /**
     * Méthode permettant de modifier un Post
     *on creait une requete SQL pour update toute une ligne
     */
    public function update($title, $author, $content, $imgPath, $id): void {
      
        $query = $this->pdo->prepare ('UPDATE Post
        SET title = :title, author = :author, content = :content,  imgPath = :imgPath WHERE id = :id');
     
        $query->bindValue(':title', $title, \PDO::PARAM_STR);
        $query->bindValue(':author', $author, \PDO::PARAM_STR);
        $query->bindValue(':content',$content , \PDO::PARAM_STR);
        $query->bindValue(':imgPath', $imgPath, \PDO::PARAM_STR);
        $query->bindValue(':id', $id, \PDO::PARAM_INT);

        $query->execute();

   }
    /**
     * Méthode permettant de récupérer un post spécifique en utilisant
     * son id. Si l'article n'existe pas, on renvoie null
     */
    public function findById(int $id): ?Post {
        //On fait la requête SELECT mais avec un WHERE pour l'id cette fois
        $query = $this->pdo->prepare('SELECT * FROM Post WHERE id=:idPlaceholder');
        //On assigne au placeholder la valeur contenu dans l'argument
        //id de la méthode
        $query->bindValue(':idPlaceholder', $id, \PDO::PARAM_INT);
        //On exécute la requête
        $query->execute();
        //On récupère le premier résultat de la requête
        $line = $query->fetch();
        //Si ce résultat existe bien
        if($line) {
            //Alors on renvoie l'instance de Post correspondante
            return $this->sqlToPost($line);
        }
        //Sinon on renvoie null pour indiquer qu'aucun post ne
        //correspondait à l'id fourni
        return null;

    }
    /**
     * Methode dont le but est de transformer une ligne de résultat
     * PDO en instance de la classe Post
     * Cette méthode est juste là dans un but de refactorisation afin
     * d'éviter la répétition dans les différents find
     */
    private function sqlToPost(array $line):Post {
        return new Post($line['title'], $line['author'],
        new \DateTime ($line['postDate']), $line['content'],
        $line['imgPath'], $line['id']);
    }

}
