<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;

class PostController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function PostTwig(Request $request)
    {
        $repo = new PostRepository();
        $newPost = null;
        //On récupère les valeurs des différents input en utilisant la request
        $title = $request->get("title");
        $author = $request->get("author");
        $content = $request->get("content");
        $imgPath = $request->get("imgPath");

        //On vérifie que chaque input contenait bien une valeur
        if ($author && $title && $content) {
            //Si c'est le cas, on utilise ces valeurs pour créer une 
            //nouvelle instance de Post qu'on met dans la variable
            $newPost = new Post($title, $author, new \DateTime, $content, $imgPath);
            $repo->add($newPost);
            return $this->redirectToRoute('home');
        }
        $postCtrl = $repo->findAll();
        return $this->render('post-twig.html.twig', ['PostCtrl' => $postCtrl]);
    }
    /**
     * On peut faire des routes avec des paramètres à l'intérieur,
     * ces paramètres devront être entourés d'accolades
     * @Route("/{id}", name="one_post")
     */
    public function onePost(int $id, Request $request)
    {

        $repo = new PostRepository();

        $author = $request->get("author");
        $title = $request->get("title");
        $content = $request->get("content");
        $imgPath = $request->get("imgPath");

        if ($author && $title && $content) {
            $repo->update($title, $author, $content, $imgPath, $id);
        }

        $post = $repo->findById($id);

        return $this->render('one-post.html.twig', [
            'post' => $post
        ]);
    }
    /**
     * On peut faire des routes avec des paramètres à l'intérieur,
     * ces paramètres devront être entourés d'accolades
     * @Route("/{id}", name="comment_post")
     */
    // public function showComment()
    // {
    //     $repo = new CommentRepository();
    // $newComment = null;

    // $author = $request->get("author");
    // $title = $request->get("title");
    // $contentPost = $request->get("content");

    // if ($author && $title && $contentPost) {
    //     $newComment = new Comment ($title, $author, $contentPost);           
    //     $repo->add($newComment);
    //     return $this->redirectToRoute('one_post');
    // }
    // $repo->findById($id);
    //     $comment = $repo->findAll();
    //     return $this->render('one-post.html.twig', [
    //         'comment' => $comment
    //     ]);
    // }

}
