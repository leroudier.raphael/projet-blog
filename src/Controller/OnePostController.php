<?php

namespace App\Controller;

use App\Repository\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\Response;

class OnePostController extends AbstractController
{
   /**
    * @Route("/del/{id}", name="delete_post")
    */
   public function deletePost(int $id)
   {
      $repo = new PostRepository();
      $repo->delete($id);
      return $this->redirectToRoute('home');
   }
    /**
    * @Route("/up/{id}", name="update_post")
    */
   public function updatePost(int $id, Request $request)
   {
      $repo = new PostRepository();
      //On récupère les valeurs des différents input en utilisant la request
      $author = $request->get("author");
      $title = $request->get("title");
      $content = $request->get("content");
      $imgPath = $request->get("imgPath");
      //On vérifie que chaque input contenait bien une valeur
      if ($author && $title && $content) {
         $repo->update($title,$author,$content, $imgPath, $id);
      }
      $postView = $repo->findById($id);
      
      return $this->render('one-post.html.twig', ['postView' => $postView]);
   }
   
}
