-- JEUX DE DONNÉES
INSERT INTO Post (title, author, content, imgPath) VALUES
('bienvenue','Rébecca', 'bienvenue voici un premier essai d\'article', "https://www.tethys-scubaclub.ch/WP/wp-content/uploads/2016/01/default-profile-pic.gif"),
-- ('presentation','Hilaire', 'le blog comportera un fil d\'article avec des contenus divers');
INSERT INTO Comment (contentPost, author, post_id) VALUES ('merci Rébecca, cordialement.','Armand', 1), ('Intéressant, merci','Hebert', 1), ('bonjour Hilaire, bonne présentation','Ribeiro', 2), ('Trop Hate', 'Camille', 2);
INSERT INTO Note (note, post_id) VALUES (3, 1), (3, 1), (2, 2), (3, 2);
